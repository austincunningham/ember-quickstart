import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    addTask: function () {
      let title = this.get('title');
      let description = this.get('description');
      let date = this.get('date');

      //alert(title + ' ' + description + ' ' + date);

      //create new task
      let newTask = this.store.createRecord('task', {
        title: title,
        description: description,
        date: new Date(date),
        type: 'json',
      });
      console.log(newTask);
      //save to Database

      let use_this = this;
      function transitionToPost(newTask) {
        console.log(newTask);
        use_this.transitionToRoute('task.new', newTask);
      }

      function failure(reason) {
        console.log(reason);
      }

      newTask.save().then(transitionToPost =>{
        console.log(transitionToPost);
      }).catch(failure =>{
        console.log(failure);
      });

      //Clear the form
      this.setProperties({
        title: '',
        description: '',
        date: '',
      });
    },
  },
});
