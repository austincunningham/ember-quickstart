//export default DS.JSONAPISerializer.extend({
//});

import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  primaryKey: '_id',
  serializeId: function (id) {
    return id.toString();
  },

  modelNameFromPayloadKey: function (payloadKey) {
    if (payloadKey === 'data') {
      return this._super(payloadKey.replace('data', 'task'));
    } else if (payloadKey === '_v') {
      return this._super(payloadKey.replace('_v', 'task'));
    } else if (payloadKey === 'id') {
      return this._super(payloadKey.replace('id', 'task'));
    } else {
      return this._super(payloadKey);
    }
  },

});

