import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr(),
  description: DS.attr(),
  date: DS.attr(),
  created: DS.attr('string', {
    defaultValue: function(){
      return new Date();
    },
  }),
});
